# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-01-12 21:08+0000\n"
"PO-Revision-Date: 2023-09-05 17:06+0000\n"
"Last-Translator: Aliaksandr Astrouski <aostrovsky87@gmail.com>\n"
"Language-Team: Russian <https://hosted.weblate.org/projects/gnu-mailman/"
"hyperkitty/ru/>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n"
"%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Weblate 5.0.1-dev\n"

#: forms.py:53
msgid "Add a tag..."
msgstr "Добавить метку..."

#: forms.py:55
msgid "Add"
msgstr "Добавить"

#: forms.py:56
msgid "use commas to add multiple tags"
msgstr "несколько меток разделите запятыми"

#: forms.py:64
msgid "Attach a file"
msgstr "Прикрепить файл"

#: forms.py:65
msgid "Attach another file"
msgstr "Прикрепить другой файл"

#: forms.py:66
msgid "Remove this file"
msgstr "Удалить файл"

#: templates/hyperkitty/404.html:28
msgid "Error 404"
msgstr "Ошибка 404"

#: templates/hyperkitty/404.html:30 templates/hyperkitty/500.html:31
msgid "Oh No!"
msgstr "О, нет!"

#: templates/hyperkitty/404.html:32
msgid "I can't find this page."
msgstr "Страница не найдена."

#: templates/hyperkitty/404.html:33 templates/hyperkitty/500.html:34
msgid "Go back home"
msgstr "Вернуться на главную страницу"

#: templates/hyperkitty/500.html:29
msgid "Error 500"
msgstr "Ошибка 500"

#: templates/hyperkitty/500.html:33
msgid "Sorry, but the requested page is unavailable due to a server hiccup."
msgstr "Извините, но запрашиваемая страница недоступна из-за сбоя сервера."

#: templates/hyperkitty/ajax/reattach_suggest.html:7
#: templates/hyperkitty/reattach.html:23
msgid "started"
msgstr "начато"

#: templates/hyperkitty/ajax/reattach_suggest.html:7
#: templates/hyperkitty/reattach.html:23
msgid "last active:"
msgstr "Последнее сообщение:"

#: templates/hyperkitty/ajax/reattach_suggest.html:8
msgid "see this thread"
msgstr "посмотреть эту ветку"

#: templates/hyperkitty/ajax/reattach_suggest.html:12
msgid "(no suggestions)"
msgstr "(нет предложений)"

#: templates/hyperkitty/ajax/temp_message.html:12
msgid "Sent just now, not yet distributed"
msgstr "Только что отправлено, еще не доставлено"

#: templates/hyperkitty/api.html:5
msgid "REST API"
msgstr "REST API"

#: templates/hyperkitty/api.html:7
msgid ""
"HyperKitty comes with a small REST API allowing you to programatically "
"retrieve emails and information."
msgstr ""
"HyperKitty идёт в комплекте с небольшим REST API, который позволяет "
"программно извлекать электронные письма и информацию."

#: templates/hyperkitty/api.html:10
msgid "Formats"
msgstr "Форматы"

#: templates/hyperkitty/api.html:12
msgid ""
"This REST API can return the information into several formats.  The default "
"format is html to allow human readibility."
msgstr ""
"Этот REST API может возвращать информацию в нескольких форматах. По "
"умолчанию используется формат HTML, что позволяет обеспечить удобство чтения "
"для человека."

#: templates/hyperkitty/api.html:14
msgid ""
"To change the format, just add <em>?format=&lt;FORMAT&gt;</em> to the URL."
msgstr ""
"Чтобы изменить формат, просто добавьте <em>?format=&lt;FORMAT&gt;</em> в URL."

#: templates/hyperkitty/api.html:16
msgid "The list of available formats is:"
msgstr "Список доступных форматов:"

#: templates/hyperkitty/api.html:20
msgid "Plain text"
msgstr "Простой текст"

#: templates/hyperkitty/api.html:26
msgid "List of mailing-lists"
msgstr "Список списков почтовой рассылки"

#: templates/hyperkitty/api.html:27 templates/hyperkitty/api.html:33
#: templates/hyperkitty/api.html:39 templates/hyperkitty/api.html:45
#: templates/hyperkitty/api.html:51
msgid "Endpoint:"
msgstr "Конечная точка (endpoint):"

#: templates/hyperkitty/api.html:29
msgid ""
"Using this address you will be able to retrieve the information known about "
"all the mailing lists."
msgstr ""
"Используя этот адрес, вы сможете получить информацию обо всех списках "
"рассылки."

#: templates/hyperkitty/api.html:32
msgid "Threads in a mailing list"
msgstr "Ветки в списке рассылки"

#: templates/hyperkitty/api.html:35
msgid ""
"Using this address you will be able to retrieve information about all the "
"threads on the specified mailing list."
msgstr ""
"Используя этот адрес, вы сможете получить информацию обо всех ветках в "
"указанном списке рассылки."

#: templates/hyperkitty/api.html:38
msgid "Emails in a thread"
msgstr "Письма в ветке"

#: templates/hyperkitty/api.html:41
msgid ""
"Using this address you will be able to retrieve the list of emails in a "
"mailing list thread."
msgstr ""
"Используя этот адрес, вы сможете получить список писем в ветке списка "
"почтовой рассылки."

#: templates/hyperkitty/api.html:44
msgid "An email in a mailing list"
msgstr "Письмо в списке почтовой рассылки"

#: templates/hyperkitty/api.html:47
msgid ""
"Using this address you will be able to retrieve the information known about "
"a specific email on the specified mailing list."
msgstr ""
"Используя этот адрес, вы сможете получить информацию о конкретном письме в "
"указанном списке рассылки."

#: templates/hyperkitty/api.html:50
msgid "Tags"
msgstr "Метки"

#: templates/hyperkitty/api.html:53
msgid "Using this address you will be able to retrieve the list of tags."
msgstr "Используя этот адрес, вы сможете получить список меток."

#: templates/hyperkitty/base.html:54 templates/hyperkitty/base.html:143
msgid "Account"
msgstr "Учетная запись"

#: templates/hyperkitty/base.html:59 templates/hyperkitty/base.html:148
msgid "Mailman settings"
msgstr "Настройки Mailman"

#: templates/hyperkitty/base.html:64 templates/hyperkitty/base.html:153
#: templates/hyperkitty/user_profile/base.html:17
msgid "Posting activity"
msgstr "Недавняя активность"

#: templates/hyperkitty/base.html:69 templates/hyperkitty/base.html:158
msgid "Logout"
msgstr "Выйти"

#: templates/hyperkitty/base.html:77 templates/hyperkitty/base.html:104
msgid "Sign In"
msgstr "Вход"

#: templates/hyperkitty/base.html:81 templates/hyperkitty/base.html:108
msgid "Sign Up"
msgstr "Регистрация"

#: templates/hyperkitty/base.html:92
msgid "Manage this list"
msgstr "Управление данным списком рассылки"

#: templates/hyperkitty/base.html:97
msgid "Manage lists"
msgstr "Управление списками рассылки"

#: templates/hyperkitty/base.html:116
msgid "Search this list"
msgstr "Поиск по этому списку"

#: templates/hyperkitty/base.html:119
msgid "Search all lists"
msgstr "Поиск по всем спискам рассылки"

#: templates/hyperkitty/base.html:194
msgid "Keyboard Shortcuts"
msgstr "Быстрые клавши"

#: templates/hyperkitty/base.html:197
msgid "Thread View"
msgstr "Режим просмотра ветки"

#: templates/hyperkitty/base.html:199
msgid "Next unread message"
msgstr "Следующее непрочитанное сообщение"

#: templates/hyperkitty/base.html:200
msgid "Previous unread message"
msgstr "Предыдущее непрочитанное сообщение"

#: templates/hyperkitty/base.html:201
msgid "Jump to all threads"
msgstr "Перейти ко всем веткам"

#: templates/hyperkitty/base.html:202
msgid "Jump to MailingList overview"
msgstr "Перейти к обзору списка рассылки"

#: templates/hyperkitty/base.html:217
msgid "Powered by"
msgstr "На основе"

#: templates/hyperkitty/base.html:217
msgid "version"
msgstr "версия"

#: templates/hyperkitty/errors/notimplemented.html:7
msgid "Not implemented yet"
msgstr "Не реализовано на данный момент"

#: templates/hyperkitty/errors/notimplemented.html:12
msgid "Not implemented"
msgstr "Не реализовано"

#: templates/hyperkitty/errors/notimplemented.html:14
msgid "This feature has not been implemented yet, sorry."
msgstr "Эта функция не реализована на данный момент, извините."

#: templates/hyperkitty/errors/private.html:7
msgid "Error: private list"
msgstr "Ошибка: закрытый список"

#: templates/hyperkitty/errors/private.html:19
msgid ""
"This mailing list is private. You must be subscribed to view the archives."
msgstr ""
"Это закрытый список рассылки. Вы должны быть подписаны на него для просмотра "
"архивов."

#: templates/hyperkitty/fragments/like_form.html:11
msgid "You like it (cancel)"
msgstr "Вам нравится это (отменить)"

#: templates/hyperkitty/fragments/like_form.html:19
msgid "You dislike it (cancel)"
msgstr "Вам не нравится это (отменить)"

#: templates/hyperkitty/fragments/like_form.html:22
#: templates/hyperkitty/fragments/like_form.html:26
msgid "You must be logged-in to vote."
msgstr "Для голосования необходимо войти."

#: templates/hyperkitty/fragments/month_list.html:7
msgid "Threads by"
msgstr "Ветки за"

#: templates/hyperkitty/fragments/month_list.html:7
msgid " month"
msgstr " месяц"

#: templates/hyperkitty/fragments/overview_threads.html:11
msgid "New messages in this thread"
msgstr "Новые сообщения в этой ветке"

#: templates/hyperkitty/fragments/overview_threads.html:38
#: templates/hyperkitty/fragments/thread_left_nav.html:19
#: templates/hyperkitty/overview.html:105
msgid "All Threads"
msgstr "Все ветки"

#: templates/hyperkitty/fragments/overview_top_posters.html:16
msgid "See the profile"
msgstr "Посмотреть профиль"

#: templates/hyperkitty/fragments/overview_top_posters.html:22
msgid "posts"
msgstr "публикаци(й)"

#: templates/hyperkitty/fragments/overview_top_posters.html:27
msgid "No posters this month (yet)."
msgstr "В этом месяце пока ничего не было опубликовано."

#: templates/hyperkitty/fragments/send_as.html:5
msgid "This message will be sent as:"
msgstr "Это сообщение будет отправлено как:"

#: templates/hyperkitty/fragments/send_as.html:6
msgid "Change sender"
msgstr "Изменить отправителя"

#: templates/hyperkitty/fragments/send_as.html:16
msgid "Link another address"
msgstr "Ссылка на другой адрес"

#: templates/hyperkitty/fragments/send_as.html:20
msgid ""
"If you aren't a current list member, sending this message will subscribe you."
msgstr ""
"Если вы еще не подписчик рассылки, отправка этого письма автоматически вас "
"подпишет."

#: templates/hyperkitty/fragments/thread_left_nav.html:12
#: templates/hyperkitty/threads/right_col.html:26
msgid "List overview"
msgstr "Обзор списка"

#: templates/hyperkitty/fragments/thread_left_nav.html:29
#: templates/hyperkitty/overview.html:116 views/message.py:76
#: views/mlist.py:114 views/thread.py:191
msgid "Download"
msgstr "Скачать"

#: templates/hyperkitty/fragments/thread_left_nav.html:32
#: templates/hyperkitty/overview.html:119
msgid "Past 30 days"
msgstr "Последние 30 дней"

#: templates/hyperkitty/fragments/thread_left_nav.html:33
#: templates/hyperkitty/overview.html:120
msgid "This month"
msgstr "Этот месяц"

#: templates/hyperkitty/fragments/thread_left_nav.html:36
#: templates/hyperkitty/overview.html:123
msgid "Entire archive"
msgstr "Весь архив"

#: templates/hyperkitty/index.html:9 templates/hyperkitty/index.html:18
msgid "Available lists"
msgstr "Доступные списки"

#: templates/hyperkitty/index.html:26
msgid "Sort by number of recent participants"
msgstr "Сортировать по количеству недавних участников"

#: templates/hyperkitty/index.html:30 templates/hyperkitty/index.html:33
#: templates/hyperkitty/index.html:88
msgid "Most popular"
msgstr "Наиболее популярные"

#: templates/hyperkitty/index.html:40
msgid "Sort by number of recent discussions"
msgstr "Сортировать по количеству недавних обсуждений"

#: templates/hyperkitty/index.html:44 templates/hyperkitty/index.html:47
#: templates/hyperkitty/index.html:91
msgid "Most active"
msgstr "Наиболее активные"

#: templates/hyperkitty/index.html:54
msgid "Sort alphabetically"
msgstr "Сортировать по алфавиту"

#: templates/hyperkitty/index.html:58 templates/hyperkitty/index.html:61
#: templates/hyperkitty/index.html:94
msgid "By name"
msgstr "По имени"

#: templates/hyperkitty/index.html:68
msgid "Sort by list creation date"
msgstr "Сортировать по дате создания списка"

#: templates/hyperkitty/index.html:72 templates/hyperkitty/index.html:75
#: templates/hyperkitty/index.html:97
msgid "Newest"
msgstr "Самые новые"

#: templates/hyperkitty/index.html:84
msgid "Sort by"
msgstr "Сортировать по"

#: templates/hyperkitty/index.html:107
msgid "Hide inactive"
msgstr "Скрыть неактивные"

#: templates/hyperkitty/index.html:108
msgid "Hide private"
msgstr "Скрыть закрытые"

#: templates/hyperkitty/index.html:115
msgid "Find list"
msgstr "Найти список"

#: templates/hyperkitty/index.html:141 templates/hyperkitty/index.html:209
#: templates/hyperkitty/user_profile/last_views.html:31
#: templates/hyperkitty/user_profile/last_views.html:70
msgid "new"
msgstr "новый"

#: templates/hyperkitty/index.html:153 templates/hyperkitty/index.html:220
msgid "private"
msgstr "закрытый"

#: templates/hyperkitty/index.html:155 templates/hyperkitty/index.html:222
msgid "inactive"
msgstr "неактивные"

#: templates/hyperkitty/index.html:161 templates/hyperkitty/index.html:247
#: templates/hyperkitty/overview.html:65 templates/hyperkitty/overview.html:72
#: templates/hyperkitty/overview.html:79 templates/hyperkitty/overview.html:88
#: templates/hyperkitty/overview.html:96 templates/hyperkitty/overview.html:146
#: templates/hyperkitty/overview.html:163 templates/hyperkitty/reattach.html:37
#: templates/hyperkitty/thread.html:85
msgid "Loading..."
msgstr "Загрузка..."

#: templates/hyperkitty/index.html:178 templates/hyperkitty/index.html:255
msgid "No archived list yet."
msgstr "Архивированных списков пока нет."

#: templates/hyperkitty/index.html:190
#: templates/hyperkitty/user_profile/favorites.html:40
#: templates/hyperkitty/user_profile/last_views.html:42
#: templates/hyperkitty/user_profile/profile.html:15
#: templates/hyperkitty/user_profile/subscriptions.html:43
#: templates/hyperkitty/user_profile/votes.html:46
msgid "List"
msgstr "Список"

#: templates/hyperkitty/index.html:191
msgid "Description"
msgstr "Описание"

#: templates/hyperkitty/index.html:192
msgid "Activity in the past 30 days"
msgstr "Пользовательская активность в последние 30 дней"

#: templates/hyperkitty/index.html:236 templates/hyperkitty/overview.html:155
#: templates/hyperkitty/thread_list.html:60
#: templates/hyperkitty/threads/right_col.html:104
#: templates/hyperkitty/threads/summary_thread_large.html:54
msgid "participants"
msgstr "участники"

#: templates/hyperkitty/index.html:241 templates/hyperkitty/overview.html:156
#: templates/hyperkitty/thread_list.html:65
msgid "discussions"
msgstr "обсуждения"

#: templates/hyperkitty/list_delete.html:7
msgid "Delete MailingList"
msgstr "Удалить архив списка рассылки"

#: templates/hyperkitty/list_delete.html:18
msgid "Delete Mailing List From HyperKitty"
msgstr "Удалить список рассылки из Hyperkitty"

#: templates/hyperkitty/list_delete.html:24
msgid ""
"will be deleted from HyperKitty along with all the threads and messages. It "
"will not be deleted from Mailman Core. Do you want to continue?"
msgstr ""
"будет удалён из HyperKitty вместе со всеми ветками и сообщениями. Он не "
"будет удалён из Mailman Core. Вы действительно хотите продолжить?"

#: templates/hyperkitty/list_delete.html:31
#: templates/hyperkitty/message_delete.html:42
msgid "Delete"
msgstr "Удалить"

#: templates/hyperkitty/list_delete.html:32
#: templates/hyperkitty/message_delete.html:43
#: templates/hyperkitty/message_new.html:51
#: templates/hyperkitty/messages/message.html:158
msgid "or"
msgstr "или"

#: templates/hyperkitty/list_delete.html:34
#: templates/hyperkitty/message_delete.html:43
#: templates/hyperkitty/message_new.html:51
#: templates/hyperkitty/messages/message.html:158
#: templates/hyperkitty/user_profile/votes.html:36
#: templates/hyperkitty/user_profile/votes.html:74
msgid "cancel"
msgstr "отмена"

#: templates/hyperkitty/message.html:20
msgid "thread"
msgstr "ветка"

#: templates/hyperkitty/message_delete.html:7
#: templates/hyperkitty/message_delete.html:18
msgid "Delete message(s)"
msgstr "Удалить сообщение(-ия)"

#: templates/hyperkitty/message_delete.html:23
#, python-format
msgid ""
"\n"
"        %(count)s message(s) will be deleted. Do you want to continue?\n"
"        "
msgstr ""
"\n"
"        %(count)s сообщение(ия)(ий) будут удалены. Хотите продолжить?\n"
"        "

#: templates/hyperkitty/message_new.html:8
#: templates/hyperkitty/message_new.html:19
msgid "Create a new thread"
msgstr "Создать новую ветку"

#: templates/hyperkitty/message_new.html:20
#: templates/hyperkitty/user_posts.html:22
msgid "in"
msgstr "в"

#: templates/hyperkitty/message_new.html:50
#: templates/hyperkitty/messages/message.html:157
msgid "Send"
msgstr "Отправить"

#: templates/hyperkitty/messages/message.html:18
#, python-format
msgid "See the profile for %(name)s"
msgstr "Посмотреть профиль для %(name)s"

#: templates/hyperkitty/messages/message.html:28
msgid "Unread"
msgstr "Непрочитанные"

#: templates/hyperkitty/messages/message.html:45
msgid "Sender's time:"
msgstr "Время отправителя:"

#: templates/hyperkitty/messages/message.html:51
msgid "New subject:"
msgstr "Новая тема:"

#: templates/hyperkitty/messages/message.html:61
msgid "Attachments:"
msgstr "Прикреплённые файлы:"

#: templates/hyperkitty/messages/message.html:76
msgid "Display in fixed font"
msgstr "Отобразить в фиксированном шрифте"

#: templates/hyperkitty/messages/message.html:81
msgid "Permalink for this message"
msgstr "Постоянная ссылка на это сообщение"

#: templates/hyperkitty/messages/message.html:92
#: templates/hyperkitty/messages/message.html:95
#: templates/hyperkitty/messages/message.html:97
msgid "Reply"
msgstr "Ответить"

#: templates/hyperkitty/messages/message.html:106
#, python-format
msgid ""
"\n"
"                %(email.attachments.count)s attachment\n"
"                "
msgid_plural ""
"\n"
"                %(email.attachments.count)s attachments\n"
"                "
msgstr[0] ""
"\n"
"                %(email.attachments.count)s прикреплённый файл\n"
"                "
msgstr[1] ""
"\n"
"                %(email.attachments.count)s прикреплённых файла\n"
"                "
msgstr[2] ""
"\n"
"                %(email.attachments.count)s прикреплённых файлов\n"
"                "
msgstr[3] ""
"\n"
"                %(email.attachments.count)s прикреплённых файлов\n"
"                "

#: templates/hyperkitty/messages/message.html:129
msgid "Sign in to reply online"
msgstr "Войти, чтобы ответить онлайн"

#: templates/hyperkitty/messages/message.html:133
#: templates/hyperkitty/messages/message.html:147
msgid "Use email software"
msgstr "Используйте клиента электронной почты"

#: templates/hyperkitty/messages/message.html:143
msgid "Quote"
msgstr "Цитировать"

#: templates/hyperkitty/messages/message.html:144
msgid "Create new thread"
msgstr "Создать новую ветку"

#: templates/hyperkitty/messages/right_col.html:11
msgid "Back to the thread"
msgstr "Вернуться к ветке"

#: templates/hyperkitty/messages/right_col.html:18
msgid "Back to the list"
msgstr "Вернуться к списку"

#: templates/hyperkitty/messages/right_col.html:27
msgid "Delete this message"
msgstr "Удалить это сообщение"

#: templates/hyperkitty/messages/summary_message.html:23
#, python-format
msgid ""
"\n"
"                                by %(name)s\n"
"                            "
msgstr ""
"\n"
"                                от %(name)s\n"
"                            "

#: templates/hyperkitty/overview.html:36
msgid "Recent"
msgstr "Недавние"

#: templates/hyperkitty/overview.html:40
msgid "Active"
msgstr "Активные"

#: templates/hyperkitty/overview.html:44
msgid "Popular"
msgstr "Популярные"

#: templates/hyperkitty/overview.html:49
#: templates/hyperkitty/user_profile/base.html:22
msgid "Favorites"
msgstr "Избранные"

#: templates/hyperkitty/overview.html:53
msgid "Posted"
msgstr "Ваши собственные"

#: templates/hyperkitty/overview.html:63
msgid "Recently active discussions"
msgstr "Последние активные обсуждения"

#: templates/hyperkitty/overview.html:70
msgid "Most popular discussions"
msgstr "Наиболее популярные обсуждения"

#: templates/hyperkitty/overview.html:77
msgid "Most active discussions"
msgstr "Наиболее активные обсуждения"

#: templates/hyperkitty/overview.html:84
msgid "Discussions You've Flagged"
msgstr "Обсуждения, которые Вы отметили"

#: templates/hyperkitty/overview.html:92
msgid "Discussions You've Posted to"
msgstr "Обсуждения, в которых Вы писали"

#: templates/hyperkitty/overview.html:109
msgid ""
"<span class=\"d-none d-md-inline\">Start a n</span><span class=\"d-md-none"
"\">N</span>ew thread"
msgstr ""
"<span class=\"d-none d-md-inline\">Создать н</span><span class=\"d-md-none"
"\">Н</span>овую ветку"

#: templates/hyperkitty/overview.html:133
msgid "Delete Archive"
msgstr "Удалить архив"

#: templates/hyperkitty/overview.html:143
msgid "Activity Summary"
msgstr "Информация о пользовательской активности"

#: templates/hyperkitty/overview.html:145
msgid "Post volume over the past <strong>30</strong> days."
msgstr "Количество публикаций за последние <strong>30</strong> дней."

#: templates/hyperkitty/overview.html:150
msgid "The following statistics are from"
msgstr "Следующие статистические данные взяты за"

#: templates/hyperkitty/overview.html:151
msgid "In"
msgstr "В"

#: templates/hyperkitty/overview.html:152
msgid "the past <strong>30</strong> days:"
msgstr "последние <strong>30</strong> дней:"

#: templates/hyperkitty/overview.html:161
msgid "Most active posters"
msgstr "Пользователи с наибольшим количеством публикаций"

#: templates/hyperkitty/overview.html:170
msgid "Prominent posters"
msgstr "Выдающиеся пользователи"

#: templates/hyperkitty/overview.html:185
msgid "kudos"
msgstr "\"спасибо\""

#: templates/hyperkitty/reattach.html:9
msgid "Reattach a thread"
msgstr "Прикрепить ветку"

#: templates/hyperkitty/reattach.html:18
msgid "Re-attach a thread to another"
msgstr "Прикрепить ветку к другой"

#: templates/hyperkitty/reattach.html:20
msgid "Thread to re-attach:"
msgstr "Прикрепляемая ветка:"

#: templates/hyperkitty/reattach.html:27
msgid "Re-attach it to:"
msgstr "Прикрепить к:"

#: templates/hyperkitty/reattach.html:29
msgid "Search for the parent thread"
msgstr "Поиск ветки более высокого уровня"

#: templates/hyperkitty/reattach.html:30
msgid "Search"
msgstr "Поиск"

#: templates/hyperkitty/reattach.html:42
msgid "this thread ID:"
msgstr "ID этой ветки:"

#: templates/hyperkitty/reattach.html:48
msgid "Do it"
msgstr "Выполнить"

#: templates/hyperkitty/reattach.html:48
msgid "(there's no undoing!), or"
msgstr "(действие невозможно будет отменить), или"

#: templates/hyperkitty/reattach.html:50
msgid "go back to the thread"
msgstr "вернуться к ветке"

#: templates/hyperkitty/search_results.html:8
msgid "Search results for"
msgstr "Результаты поиска для"

#: templates/hyperkitty/search_results.html:28
msgid "search results"
msgstr "результаты поиска"

#: templates/hyperkitty/search_results.html:30
msgid "Search results"
msgstr "Результаты поиска"

#: templates/hyperkitty/search_results.html:32
msgid "for query"
msgstr "для запроса"

#: templates/hyperkitty/search_results.html:42
#: templates/hyperkitty/user_posts.html:34
msgid "messages"
msgstr "сообщения"

#: templates/hyperkitty/search_results.html:55
msgid "sort by score"
msgstr "сортировать по баллам"

#: templates/hyperkitty/search_results.html:58
msgid "sort by latest first"
msgstr "сначала более поздние"

#: templates/hyperkitty/search_results.html:61
msgid "sort by earliest first"
msgstr "сначала более ранние"

#: templates/hyperkitty/search_results.html:82
msgid "Sorry no email could be found for this query."
msgstr "К сожалению, по этому запросу не было найдено ни одного письма."

#: templates/hyperkitty/search_results.html:85
msgid "Sorry but your query looks empty."
msgstr "Извините, но ваш запрос выглядит пустым."

#: templates/hyperkitty/search_results.html:86
msgid "these are not the messages you are looking for"
msgstr "это не те сообщения, которые Вы ищете"

#: templates/hyperkitty/thread.html:26
msgid "newer"
msgstr "более новые"

#: templates/hyperkitty/thread.html:45
msgid "older"
msgstr "более старые"

#: templates/hyperkitty/thread.html:71
msgid "Show replies by thread"
msgstr "Показать ответы по ветке"

#: templates/hyperkitty/thread.html:74
msgid "Show replies by date"
msgstr "Показать ответы по дате"

#: templates/hyperkitty/thread.html:87
msgid "Visit here for a non-javascript version of this page."
msgstr "Нажмите сюда для просмотра этой страницы без javascript."

#: templates/hyperkitty/thread_list.html:37
#: templates/hyperkitty/user_profile/profile.html:19
msgid "Thread"
msgstr "Ветка"

#: templates/hyperkitty/thread_list.html:38
msgid "Start a new thread"
msgstr "Начать новую тему"

#: templates/hyperkitty/thread_list.html:74
msgid "Sorry no email threads could be found"
msgstr "К сожалению, не было найдено ни одной ветки почты"

#: templates/hyperkitty/threads/category.html:7
msgid "Click to edit"
msgstr "Нажмите, чтобы отредактировать"

#: templates/hyperkitty/threads/category.html:9
msgid "You must be logged-in to edit."
msgstr "Для редактирования необходимо войти."

#: templates/hyperkitty/threads/category.html:15
msgid "no category"
msgstr "нет категории"

#: templates/hyperkitty/threads/right_col.html:13
msgid "Age (days ago)"
msgstr "Возраст (дней назад)"

#: templates/hyperkitty/threads/right_col.html:19
msgid "Last active (days ago)"
msgstr "Последняя активность (дней назад)"

#: templates/hyperkitty/threads/right_col.html:47
#, python-format
msgid "%(num_comments)s comments"
msgstr "%(num_comments)s комментариев"

#: templates/hyperkitty/threads/right_col.html:51
#, python-format
msgid "%(participants_count)s participants"
msgstr "%(participants_count)s участников"

#: templates/hyperkitty/threads/right_col.html:56
#, python-format
msgid "%(unread_count)s unread <span class=\"hidden-sm\">messages</span>"
msgstr ""
"%(unread_count)s непрочитанных <span class=\"hidden-sm\">сообщений</span>"

#: templates/hyperkitty/threads/right_col.html:66
msgid "You must be logged-in to have favorites."
msgstr "Для доступа к избранному необходимо войти."

#: templates/hyperkitty/threads/right_col.html:67
msgid "Add to favorites"
msgstr "Добавить в избранное"

#: templates/hyperkitty/threads/right_col.html:69
msgid "Remove from favorites"
msgstr "Удалить из избранного"

#: templates/hyperkitty/threads/right_col.html:78
msgid "Reattach this thread"
msgstr "Прикрепить эту ветку"

#: templates/hyperkitty/threads/right_col.html:82
msgid "Delete this thread"
msgstr "Удалить эту ветку"

#: templates/hyperkitty/threads/right_col.html:122
msgid "Unreads:"
msgstr "Непрочитанных:"

#: templates/hyperkitty/threads/right_col.html:124
msgid "Go to:"
msgstr "Перейти к:"

#: templates/hyperkitty/threads/right_col.html:124
msgid "next"
msgstr "далее"

#: templates/hyperkitty/threads/right_col.html:125
msgid "prev"
msgstr "назад"

#: templates/hyperkitty/threads/summary_thread_large.html:21
#: templates/hyperkitty/threads/summary_thread_large.html:23
msgid "Favorite"
msgstr "Избранное"

#: templates/hyperkitty/threads/summary_thread_large.html:38
msgid "Most recent thread activity"
msgstr "Последняя пользовательская активность в ветке"

#: templates/hyperkitty/threads/summary_thread_large.html:59
msgid "comments"
msgstr "комментарии"

#: templates/hyperkitty/threads/tags.html:3
msgid "tags"
msgstr "метки"

#: templates/hyperkitty/threads/tags.html:9
msgid "Search for tag"
msgstr "Поиск по метке"

#: templates/hyperkitty/threads/tags.html:15
msgid "Remove"
msgstr "Удалить"

#: templates/hyperkitty/user_posts.html:8
#: templates/hyperkitty/user_posts.html:21
#: templates/hyperkitty/user_posts.html:25
msgid "Messages by"
msgstr "Сообщения от"

#: templates/hyperkitty/user_posts.html:38
#, python-format
msgid "Back to %(fullname)s's profile"
msgstr "Вернуться к профилю %(fullname)s"

#: templates/hyperkitty/user_posts.html:48
msgid "Sorry no email could be found by this user."
msgstr "К сожалению, не было найдено ни одного письма от этого пользователя."

#: templates/hyperkitty/user_profile/base.html:5
#: templates/hyperkitty/user_profile/base.html:12
msgid "User posting activity"
msgstr "История публикаций пользователя"

#: templates/hyperkitty/user_profile/base.html:12
#: templates/hyperkitty/user_public_profile.html:7
#: templates/hyperkitty/user_public_profile.html:14
msgid "for"
msgstr "для"

#: templates/hyperkitty/user_profile/base.html:26
msgid "Threads you have read"
msgstr "Ветки, которые вы прочли"

#: templates/hyperkitty/user_profile/base.html:30
#: templates/hyperkitty/user_profile/profile.html:18
#: templates/hyperkitty/user_profile/subscriptions.html:47
msgid "Votes"
msgstr "Голоса"

#: templates/hyperkitty/user_profile/base.html:34
msgid "Subscriptions"
msgstr "Подписки"

#: templates/hyperkitty/user_profile/favorites.html:24
#: templates/hyperkitty/user_profile/last_views.html:24
#: templates/hyperkitty/user_profile/votes.html:23
msgid "Original author:"
msgstr "Первоначальный автор:"

#: templates/hyperkitty/user_profile/favorites.html:26
#: templates/hyperkitty/user_profile/last_views.html:26
#: templates/hyperkitty/user_profile/votes.html:25
msgid "Started on:"
msgstr "Время начала:"

#: templates/hyperkitty/user_profile/favorites.html:28
#: templates/hyperkitty/user_profile/last_views.html:28
msgid "Last activity:"
msgstr "Последнее действие:"

#: templates/hyperkitty/user_profile/favorites.html:30
#: templates/hyperkitty/user_profile/last_views.html:30
msgid "Replies:"
msgstr "Ответы:"

#: templates/hyperkitty/user_profile/favorites.html:41
#: templates/hyperkitty/user_profile/last_views.html:43
#: templates/hyperkitty/user_profile/profile.html:16
#: templates/hyperkitty/user_profile/votes.html:47
msgid "Subject"
msgstr "Тема"

#: templates/hyperkitty/user_profile/favorites.html:42
#: templates/hyperkitty/user_profile/last_views.html:44
#: templates/hyperkitty/user_profile/votes.html:48
msgid "Original author"
msgstr "Первоначальный автор"

#: templates/hyperkitty/user_profile/favorites.html:43
#: templates/hyperkitty/user_profile/last_views.html:45
#: templates/hyperkitty/user_profile/votes.html:49
msgid "Start date"
msgstr "Дата начала"

#: templates/hyperkitty/user_profile/favorites.html:44
#: templates/hyperkitty/user_profile/last_views.html:46
msgid "Last activity"
msgstr "Последняя публикация"

#: templates/hyperkitty/user_profile/favorites.html:45
#: templates/hyperkitty/user_profile/last_views.html:47
msgid "Replies"
msgstr "Ответы"

#: templates/hyperkitty/user_profile/favorites.html:71
msgid "No favorites yet."
msgstr "Пока что в избранном ничего нет."

#: templates/hyperkitty/user_profile/last_views.html:56
msgid "New comments"
msgstr "Новые комментарии"

#: templates/hyperkitty/user_profile/last_views.html:79
msgid "Nothing read yet."
msgstr "Пока что ничего не прочитано."

#: templates/hyperkitty/user_profile/profile.html:9
msgid "Last posts"
msgstr "Последние публикации"

#: templates/hyperkitty/user_profile/profile.html:17
msgid "Date"
msgstr "Дата"

#: templates/hyperkitty/user_profile/profile.html:20
msgid "Last thread activity"
msgstr "Последняя публикация в ветке"

#: templates/hyperkitty/user_profile/profile.html:51
msgid "No posts yet."
msgstr "Пока что публикаций нет."

#: templates/hyperkitty/user_profile/subscriptions.html:24
msgid "since first post"
msgstr "с первой публикации"

#: templates/hyperkitty/user_profile/subscriptions.html:26
#: templates/hyperkitty/user_profile/subscriptions.html:65
msgid "post"
msgstr "публикация"

#: templates/hyperkitty/user_profile/subscriptions.html:33
#: templates/hyperkitty/user_profile/subscriptions.html:73
msgid "no post yet"
msgstr "пока что публикаций нет"

#: templates/hyperkitty/user_profile/subscriptions.html:44
msgid "Time since the first activity"
msgstr "Время с первой публикации"

#: templates/hyperkitty/user_profile/subscriptions.html:45
msgid "First post"
msgstr "Первая публикация"

#: templates/hyperkitty/user_profile/subscriptions.html:46
msgid "Posts to this list"
msgstr "Публикации в этом списке"

#: templates/hyperkitty/user_profile/subscriptions.html:80
msgid "no subscriptions"
msgstr "нет подписок"

#: templates/hyperkitty/user_profile/votes.html:32
#: templates/hyperkitty/user_profile/votes.html:70
msgid "You like it"
msgstr "Вам это нравится"

#: templates/hyperkitty/user_profile/votes.html:34
#: templates/hyperkitty/user_profile/votes.html:72
msgid "You dislike it"
msgstr "Вам это не нравится"

#: templates/hyperkitty/user_profile/votes.html:50
msgid "Vote"
msgstr "Голосование"

#: templates/hyperkitty/user_profile/votes.html:83
msgid "No vote yet."
msgstr "Голосов пока нет."

#: templates/hyperkitty/user_public_profile.html:7
msgid "User Profile"
msgstr "Профиль пользователя"

#: templates/hyperkitty/user_public_profile.html:14
msgid "User profile"
msgstr "Профиль пользователя"

#: templates/hyperkitty/user_public_profile.html:23
msgid "Name:"
msgstr "Имя:"

#: templates/hyperkitty/user_public_profile.html:28
msgid "Creation:"
msgstr "Создание:"

#: templates/hyperkitty/user_public_profile.html:33
msgid "Votes for this user:"
msgstr "Голосов за этого пользователя:"

#: templates/hyperkitty/user_public_profile.html:43
msgid "Email addresses:"
msgstr "Адреса почты:"

#: views/message.py:77
msgid "This message in gzipped mbox format"
msgstr "Это сообщение в архиве (gzip) в формате mbox"

#: views/message.py:206
msgid "Your reply has been sent and is being processed."
msgstr "Ваш ответ отправлен и обрабатывается."

#: views/message.py:210
msgid ""
"\n"
"  You have been subscribed to {} list."
msgstr ""
"\n"
"  Вы подписаны на список рассылки {}."

#: views/message.py:302
#, python-format
msgid "Could not delete message %(msg_id_hash)s: %(error)s"
msgstr "Не удалось удалить сообщение %(msg_id_hash)s: %(error)s"

#: views/message.py:311
#, python-format
msgid "Successfully deleted %(count)s messages."
msgstr "Успешно удалено %(count)s сообщений."

#: views/mlist.py:88
msgid "for this MailingList"
msgstr "для этого списка рассылки"

#: views/mlist.py:100
msgid "for this month"
msgstr "за этот месяц"

#: views/mlist.py:103
msgid "for this day"
msgstr "за этот день"

#: views/mlist.py:115
msgid "This month in gzipped mbox format"
msgstr "За этот месяц в архиве (gzip) в формате mbox"

#: views/mlist.py:250 views/mlist.py:274
msgid "No discussions this month (yet)."
msgstr "В этом месяце пока что не было обсуждений."

#: views/mlist.py:262
msgid "No vote has been cast this month (yet)."
msgstr "В этом месяце пока что никто не голосовал."

#: views/mlist.py:291
msgid "You have not flagged any discussions (yet)."
msgstr "Вы пока что не отметили ни одного обсуждения."

#: views/mlist.py:314
msgid "You have not posted to this list (yet)."
msgstr "Вы пока что ничего не опубликовали в этом списке."

#: views/mlist.py:407
msgid "You must be a staff member to delete a MailingList"
msgstr "Вы должны быть администратором для удаления списка рассылки"

#: views/mlist.py:421
msgid "Successfully deleted {}"
msgstr "Успешно удалено сообщений: {}"

#: views/search.py:115
#, python-format
msgid "Parsing error: %(error)s"
msgstr "Ошибка разбора: %(error)s"

#: views/thread.py:192
msgid "This thread in gzipped mbox format"
msgstr "Эта ветка в архиве (gzip) в формате mbox"

#~ msgid "You must be logged-in to create a thread."
#~ msgstr "Для создания ветки необходимо войти."

#~ msgid "Most Active"
#~ msgstr "Наиболее активные"

#~ msgid "Home"
#~ msgstr "Главная страница"

#~ msgid "Stats"
#~ msgstr "Статистика"

#~ msgid "Threads"
#~ msgstr "Ветки"

#, fuzzy
#~| msgid "new"
#~ msgid "New"
#~ msgstr "новый"

#~ msgid ""
#~ "<span class=\"d-none d-md-inline\">Manage s</span><span class=\"d-md-none"
#~ "\">S</span>ubscription"
#~ msgstr ""
#~ "<span class=\"d-none d-md-inline\">Управление п</span><span class=\"d-md-"
#~ "none\">П</span>одпиской"

#~ msgid "First Post"
#~ msgstr "Первая публикация"

#~ msgid "days inactive"
#~ msgstr "дней без пользовательской активности"

#~ msgid "days old"
#~ msgstr "дней назад"

#~ msgid ""
#~ "\n"
#~ "                    by %(name)s\n"
#~ "                    "
#~ msgstr ""
#~ "\n"
#~ "                    от %(name)s\n"
#~ "                    "

#~ msgid "unread"
#~ msgstr "непрочитанные"

#~ msgid "Go to"
#~ msgstr "Перейти в"

#~ msgid "More..."
#~ msgstr "Больше…"

#~ msgid "Discussions"
#~ msgstr "Обсуждения"

#, fuzzy
#~ msgid "most recent"
#~ msgstr "последние"

#~ msgid "most popular"
#~ msgstr "наиболее популярные"

#~ msgid "most active"
#~ msgstr "наиболее активные"

#~ msgid "Update"
#~ msgstr "Обновление"

#, fuzzy
#~ msgid ""
#~ "\n"
#~ "                                        by %(name)s\n"
#~ "                                    "
#~ msgstr ""
#~ "\n"
#~ "                                        от  %(name)s\n"
#~ "                                    "
